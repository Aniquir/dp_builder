package ClassicBuilder.entities;

public class Things {

    private String thing;
    private int quantity;

    public String getThing() {
        return thing;
    }

    public void setThing(String thing) {
        this.thing = thing;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Things{" +
                "thing='" + thing + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
