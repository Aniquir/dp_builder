package ClassicBuilder.entities;

public class Windows {

    private int quantity;
    private String color;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Windows{" +
                "quantity=" + quantity +
                ", color='" + color + '\'' +
                '}';
    }
}
