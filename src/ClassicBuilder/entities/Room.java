package ClassicBuilder.entities;

public class Room {

    private Things things;
    private Windows windows;

    public Things getThings() {
        return things;
    }

    public void setThings(Things things) {
        this.things = things;
    }

    public Windows getWindows() {
        return windows;
    }

    public void setWindows(Windows windows) {
        this.windows = windows;
    }

    @Override
    public String toString() {
        return "Room{" +
                "things=" + things +
                ", windows=" + windows +
                '}';
    }
}
