package ClassicBuilder.builder;

import ClassicBuilder.entities.Room;

public interface RoomBuilder {

    public void BuildThings();

    public void BuildWindows();

    public Room getRoom();


}
