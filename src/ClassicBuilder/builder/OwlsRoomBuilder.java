package ClassicBuilder.builder;

import ClassicBuilder.entities.Room;
import ClassicBuilder.entities.Things;
import ClassicBuilder.entities.Windows;

public class OwlsRoomBuilder implements RoomBuilder{

    private Room room;

    public OwlsRoomBuilder() {
        this.room = new Room();
    }

    @Override
    public void BuildThings() {
        Things things = new Things();
        things.setThing("smallLibrary");
        things.setQuantity(1);

        room.setThings(things);
    }

    @Override
    public void BuildWindows() {
        Windows windows = new Windows();
        windows.setQuantity(3);
        windows.setColor("white");

        room.setWindows(windows);
    }

    @Override
    public Room getRoom() {
        return room;
    }
}
