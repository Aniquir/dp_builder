package ClassicBuilder.builder;

import ClassicBuilder.entities.Room;

public class RoomDirector {

    private RoomBuilder roomBuilder;

    public RoomDirector(RoomBuilder roomBuilder) {
        this.roomBuilder = roomBuilder;
    }

    public void makeRoom(){
        roomBuilder.BuildThings();
        roomBuilder.BuildWindows();
    }
    public Room getRoom(){
        return this.roomBuilder.getRoom();
    }
}
