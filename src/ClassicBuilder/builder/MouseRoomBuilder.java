package ClassicBuilder.builder;

import ClassicBuilder.entities.Room;
import ClassicBuilder.entities.Things;
import ClassicBuilder.entities.Windows;

public class MouseRoomBuilder implements RoomBuilder{

    Room room;

    public MouseRoomBuilder(){
        this.room = new Room();
    }

    @Override
    public void BuildThings() {
        Things things = new Things();
        things.setThing("cheese");
        things.setQuantity(5);

        room.setThings(things);
    }

    @Override
    public void BuildWindows() {
        Windows windows = new Windows();
        windows.setQuantity(1);
        windows.setColor("gray");

        room.setWindows(windows);
    }

    @Override
    public Room getRoom() {
        return room;
    }
}
