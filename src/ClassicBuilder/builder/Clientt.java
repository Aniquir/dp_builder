package ClassicBuilder.builder;

import ClassicBuilder.entities.Room;

public class Clientt {

    public static void main(String[] args) {

        //first example
        RoomBuilder roomBuilder = new OwlsRoomBuilder();
        RoomDirector roomDirector = new RoomDirector(roomBuilder);
        roomDirector.makeRoom();

        Room room = roomDirector.getRoom();
        System.out.println(room);

        //second example
        RoomBuilder roomBuilder1 = new MouseRoomBuilder();
        RoomDirector roomDirector1 = new RoomDirector(roomBuilder1);
        roomDirector1.makeRoom();

        Room room1 = roomDirector1.getRoom();
        System.out.println(room1);


    }
}
