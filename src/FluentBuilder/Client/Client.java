package FluentBuilder.Client;

import FluentBuilder.first.Dog;
import FluentBuilder.first.ProblemDog;
import FluentBuilder.second.Car;
import FluentBuilder.third.Junior;

public class Client {

    public static void main(String[] args) {

        //without builder
        ProblemDog problemDog = new ProblemDog("Stejk", 12, "Olga");
        System.out.println(problemDog);

        ProblemDog problemDog2 = new ProblemDog("Klops");
        problemDog2.setAge(3);
        problemDog2.setOwner("Olga");
        System.out.println(problemDog2);

        //first builder
        Dog dog = new Dog.Builder("Stejk")
                .age(12)
                .owner("Olga")
                .build();
        System.out.println(dog);

        Dog dog2 = new Dog.Builder("Klops")
                .age(3)
                .owner("Olga")
                .build();
        System.out.println(dog2);

        //second builder
        Car car = new Car.Builder()
                .color("Black")
                .age(10)
                .build();
        System.out.println(car);

        //third builder
        Junior junior = new Junior.Builder()
                .name("Pawel")
                .age(26)
                .skill("knowledge about Builder")
                .build();
        System.out.println(junior);

    }
}
