package FluentBuilder.third;

public class Junior {

    private String name;
    private int age;
    private String skill;

    private Junior(Builder builder){
        this.name = builder.name;
        this.age = builder.age;
        this.skill = builder.skill;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getSkill() {
        return skill;
    }

    public static class Builder{

        private String name;
        private int age;
        private String skill;


    public Builder name(String name){
        this.name = name;
        return this;
    }
    public Builder age(int age){
        this.age = age;
        return this;
    }
    public Builder skill(String skill){
        this.skill = skill;
        return this;
    }
    public Junior build(){
        return new Junior(this);
    }

    }

    @Override
    public String toString() {
        return "Junior{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", skill='" + skill + '\'' +
                '}';
    }
}
